# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 

### Lancement du projet avec Docker

Créez dans un premier temps les fichiers nécessaires à la dockerisation. Ces fichiers seront crées à la racine
de notre projet :
- un fichier .gitlab-ci.yml
- un fichier Dockerfile

/!\ Ne pas oublier de git add, commit & push après les modifications apportées à ces fichiers ;

Executez ensuite le package par le biais de l'utilitaire maven ;

Executez la commande suivante dans le terminal, ce qui permettra de créer une image de notre projet :
docker build -t hello . ;

On crée ensuite un réseau pour notre container :
docker network create reseau-hello ;

Executez la commande pour lancer le projet sur le port 8181 :
docker run --name ContainerHello -p 8181:8080 -d Hello

A ce stade, vérifiez sur Docker Desktop si l'image et le container ont bien été déployé.

Se rendre sur GitLab afin de vérifier si la pipeline s'est correctement exécutée, rubrique CI/CD -> Pieplines.